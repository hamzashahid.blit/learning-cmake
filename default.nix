let pkgs = import <nixpkgs> {};
in
pkgs.stdenv.mkDerivation rec {
	pname = "learning-cmake";
	version = "0.1";

	src = pkgs.fetchFromGitLab {
		owner = "hamzashahid.blit";
		repo = "learning-cmake";
		rev = "44293c9e83f17bb0f03cb96e0f33b47902680be3";
		sha256 = "sha256:02mgx5x6pl7rn8y63f8yz2bk6j17fl97pwcvq0m2zrihsdiwqss0";
	};

    buildInputs = with pkgs; [ gcc gdb cmake xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXi xorg.libXext libGL glfw glew ];

	configurePhase = ''
		substituteInPlace ./CMakeLists.txt \
			--replace "add_subdirectory(external/glfw)" \
					  "#add_subdirectory(external/glfw)"

		substituteInPlace ./CMakeLists.txt \
			--replace "PUBLIC external/glfw/include" \
					  "#PUBLIC external/glfw/include"

		substituteInPlace ./CMakeLists.txt \
			--replace "PRIVATE external/glfw/src" \
					  "#PRIVATE external/glfw/src"

		substituteInPlace ./CMakeLists.txt \
			--replace "message(FATAL_ERROR \"The GLFW submodules" \
					  "message(\"The GLFW submodules"
		
		./configure.sh
	'';

	buildPhase = ''
		./build.sh
	'';

	installPhase = ''	
		mkdir -p $out/bin	
		cp out/build/HelloWorld $out/bin
	'';
}
