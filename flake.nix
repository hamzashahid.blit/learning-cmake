{
  description = "A Flake for learning CMake on NixOS";

  inputs = {
	nixpkgs.url = "github:nixos/nixpkgs";
	learning-cmake = {
	  url = "gitlab:hamzashahid.blit/learning-cmake";
	  flake = true;
	};
  };

  outputs = { self, nixpkgs, learning-cmake }:
	let pkgs = nixpkgs.legacyPackages.x86_64-linux;
	in {
	  learningCMakeDrv = pkgs.callPackage ./learning-cmake.nix { newSrc = learning-cmake; };
	  defaultPackage.x86_64-linux = self.learningCMakeDrv;

	  devShell.x86_64-linux =
		pkgs.mkShell {
		  buildInputs = with pkgs; [ gcc gdb cmake xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXi xorg.libXext libGL glfw glew ];
		};
    };
}

#packages.x86_64-linux.hello = pkgs.hello;
#defaultPackage.x86_64-linux = self.packages.x86_64-linux.hello;
