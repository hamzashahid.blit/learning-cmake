#include <iostream>
#include <adder.h>
#include <GL/glew.h>
//#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <HelloWorldConfig.h>

int main(int argc, char* argv[]){

    /* Create a windowed mode window and its OpenGL context */

	std::cout << "Hello, You are using: " << argv[0] << " Version " << HelloWorld_VERSION_MAJOR << "." << HelloWorld_VERSION_MINOR << "\n";

    std::cout << "Hello! Here is my float adder adding 23.4 + 56.3: " << "\n";
	std::cout << floatAdder(23.4, 56.3) << "\n";

	GLFWwindow* window;


    /* Initialize the library */
	if(!glfwInit()) return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        //glClear(GL_COLOR_BUFFER_BIT);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}
